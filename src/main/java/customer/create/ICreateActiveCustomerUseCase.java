package customer.create;


import customer.ValidationError;
import customer.dto.ActiveCustomerDto;
import io.vavr.control.Either;

// #region interface
public interface ICreateActiveCustomerUseCase {

    Either<ValidationError, ActiveCustomerDto> create(ActiveCustomerDto request);

}
// #endregion interface
