package customer.create;


import customer.IPresenter;
import customer.ValidationError;
import customer.dto.ActiveCustomerDto;

public interface ICreatedCustomerPresenter<R> extends IPresenter<ValidationError, ActiveCustomerDto, R> {


}
