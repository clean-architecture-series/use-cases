package customer.create;


import customer.dto.ActiveCustomerDto;

public interface ISaveActiveCustomer {
    ActiveCustomerDto save(ActiveCustomerDto activeCustomer);
}
