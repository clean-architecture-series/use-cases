package customer;


public interface IPresenter<F, S, R> {

    R presentSuccess(S success);

    R presentDomainFailure(F failure);
}
