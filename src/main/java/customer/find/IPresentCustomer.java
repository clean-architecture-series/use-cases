package customer.find;


import customer.IPresenter;
import customer.ValidationError;
import customer.dto.ICustomerDto;

public interface IPresentCustomer<R> extends IPresenter<ValidationError, ICustomerDto, R> {


}
