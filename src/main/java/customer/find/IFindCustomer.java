package customer.find;


import customer.dto.ICustomerDto;
import io.vavr.control.Option;

public interface IFindCustomer {
    Option<ICustomerDto> findById(Long nationalId);
}
