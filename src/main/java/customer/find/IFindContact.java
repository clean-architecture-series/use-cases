package customer.find;

import customer.dto.ICustomerDto;
import io.vavr.control.Option;


public interface IFindContact {

    Option<ICustomerDto> findById(String id);
}
