package customer.find;


import customer.ValidationError;
import customer.dto.ICustomerDto;
import io.vavr.control.Either;

public interface IFindCustomerUseCase {

    Either<ValidationError, ICustomerDto> findById(Long nationalId);


}
