package customer.unsubscribe;


import customer.IPresenter;
import customer.ValidationError;
import customer.dto.UnsubscribedCustomerDto;

public interface IUnsubscribedCustomerPresenter<R> extends IPresenter<ValidationError, UnsubscribedCustomerDto, R> {


}
