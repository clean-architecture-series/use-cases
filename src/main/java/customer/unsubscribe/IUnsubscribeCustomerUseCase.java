package customer.unsubscribe;


import customer.ValidationError;
import customer.dto.UnsubscribedCustomerDto;
import io.vavr.control.Either;

// #region interface
public interface IUnsubscribeCustomerUseCase {
    Either<ValidationError, UnsubscribedCustomerDto> unsubscribe(UnsubscribedCustomerDto dto);
}
// #endregion interface
