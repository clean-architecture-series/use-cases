package customer.unsubscribe;


import customer.dto.UnsubscribedCustomerDto;

public interface ISaveUnsubscribedCustomer {
    UnsubscribedCustomerDto save(UnsubscribedCustomerDto dto);
}
