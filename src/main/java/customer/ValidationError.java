package customer;


import io.vavr.collection.Seq;

import java.util.List;
import java.util.function.Function;


public record ValidationError(List<String> errors) {


    public static ValidationError of(String message) {
        return new ValidationError(List.of(message));
    }

    public static ValidationError of(Seq<String> messages) {
        return new ValidationError(messages.asJava());
    }

    public static ValidationError ofAll(Seq<Seq<String>> messages) {
        return new ValidationError(messages.flatMap(Function.identity()).asJava());
    }

}
