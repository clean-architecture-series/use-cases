package customer.dto;


import lombok.Builder;

@Builder
public record UnsubscribedCustomerDto(Long id, String reason) implements ICustomerDto {


}
