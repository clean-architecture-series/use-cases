package customer.dto;

public sealed interface ICustomerDto permits ActiveCustomerDto, UnsubscribedCustomerDto {

    Long id();
}
