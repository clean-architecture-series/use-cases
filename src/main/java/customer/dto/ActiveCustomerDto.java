package customer.dto;


import customer.dto.contact.IContactDto;
import lombok.Builder;

import java.util.Set;

@Builder
public record ActiveCustomerDto(Long id,
                                String name,
                                String middleName,
                                String familyName,
                                Set<IContactDto> contacts) implements ICustomerDto {


}
