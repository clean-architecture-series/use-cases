package customer.update;


import customer.IPresenter;
import customer.ValidationError;
import customer.dto.ActiveCustomerDto;

public interface IUpdatedCustomerPresenter<R> extends IPresenter<ValidationError, ActiveCustomerDto, R> {


}
