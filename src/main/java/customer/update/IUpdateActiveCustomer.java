package customer.update;


import customer.dto.ActiveCustomerDto;

public interface IUpdateActiveCustomer {
    ActiveCustomerDto update(ActiveCustomerDto activeCustomer);
}
