package customer.update;


import customer.ValidationError;
import customer.dto.ActiveCustomerDto;
import io.vavr.control.Either;

// #region interface
public interface IUpdateCustomerUseCase {
    Either<ValidationError, ActiveCustomerDto> update(ActiveCustomerDto dto);
}
// #endregion interface